package am.egs.rest.service.impl;

import am.egs.rest.model.Role;
import am.egs.rest.repository.RoleRepository;
import am.egs.rest.util.exceptions.AccessDeniedException;
import javassist.NotFoundException;
import am.egs.rest.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import am.egs.rest.repository.UserRepository;
import am.egs.rest.service.UserService;

import java.util.ArrayList;
import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    @Override
    public void signUp(User user) throws AccessDeniedException {
        if (userRepository.getUserByEmail(user.getEmail()) != null ){
            throw new AccessDeniedException("user is exist");
        }

        Role roleUser = roleRepository.findRoleByRole("USER");
        List<Role> roles = new ArrayList<>();
        roles.add(roleUser);

        user.setRoles(roles);
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        userRepository.save(user);
    }

    @Override
    public void delete(String email) throws NotFoundException {
        User user = userRepository.getUserByEmail(email);
        if (user == null) {
            throw new NotFoundException("user does not exist");
        }
        userRepository.delete(user);
    }

    @Override
    public void update(User user) throws NotFoundException {
       User user1 = userRepository.getUserByEmail(user.getEmail());
        if (user == null){
            throw new NotFoundException("user do nor exist");
        }
        user1.setName(user.getName());
        user1.setSurname(user.getSurname());
        user1.setAge(user.getAge());
        user1.setEmail(user.getEmail());
        user1.setPassword(user.getPassword());
        userRepository.save(user1);
    }

    @Override
    public User getUser(String email) {
        return userRepository.getUserByEmail(email);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return null;
    }
}
