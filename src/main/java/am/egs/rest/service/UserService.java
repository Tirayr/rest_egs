package am.egs.rest.service;

import am.egs.rest.util.exceptions.AccessDeniedException;
import javassist.NotFoundException;
import am.egs.rest.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    void signUp(User user) throws AccessDeniedException;

    void delete(String email) throws NotFoundException;

    void update(User user) throws NotFoundException;

    User getUser(String email);

    List<User> getAllUsers();

}
