package am.egs.rest.controller;


import javassist.NotFoundException;
import am.egs.rest.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import am.egs.rest.service.UserService;
import am.egs.rest.util.exceptions.AccessDeniedException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /*
     *    add user
     * */
    @PostMapping("/registration")
    public ResponseEntity signUp(@RequestBody @Valid User user) throws AccessDeniedException {
        userService.signUp(user);
        return ResponseEntity.ok().build();
    }



    /*
     *    find user
     * */

    @GetMapping("/getAll")
    List<User> findAll(){
        return userService.getAllUsers();
    }




    @GetMapping("/login")
    public ResponseEntity login(@RequestHeader String email,
                                @RequestHeader String password) throws NotFoundException, AccessDeniedException {

        User user = userService.getUser(email);
        if (user == null) {
            throw new NotFoundException("please sign up");
        }
        if (!password.equals(user.getPassword())) {
            throw new AccessDeniedException("wrong password");
        }
        System.out.println(user.toString());
        return ResponseEntity.ok().build();
    }

    /*
     *    delete user
     * */
    @PutMapping("/delete")
    public ResponseEntity delete(@RequestParam String email) throws NotFoundException {
        userService.delete(email);
        return ResponseEntity.ok().build();
    }


    /*
     *    update user
     * */
    @PutMapping("/update")
    public ResponseEntity update(@RequestBody User user) throws NotFoundException {
        userService.update(user);
        return ResponseEntity.ok().build();
    }


}
